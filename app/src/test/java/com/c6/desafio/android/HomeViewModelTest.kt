package com.c6.desafio.android

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.jraska.livedata.test
import com.c6.desafio.android.common.Resource
import com.c6.desafio.android.di.remoteModule
import com.c6.desafio.android.di.repositoryModule
import com.c6.desafio.android.di.servicesModule
import com.c6.desafio.android.di.uiModule
import com.c6.desafio.android.home.MainViewModel
import com.c6.desafio.android.home.interactor.ListUsersUseCase
import com.c6.desafio.android.home.model.ResponseListRepo
import com.c6.desafio.android.home.service.C6ServiceMock
import io.mockk.mockk
import junit.framework.Assert.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.koin.test.mock.declare
import org.mockito.Mockito
import org.mockito.Mockito.`when`


class HomeViewModelTest: AutoCloseKoinTest() {

    private fun viewModel(): MainViewModel {
        val viewModel = MainViewModel(get())
        viewModel.repo.observeForever(onDataLoadedObserver)
        return viewModel
    }
    private val onDataLoadedObserver = mockk<Observer<Resource<ResponseListRepo>>>(relaxed = true)
    private val listUsersUseCase: ListUsersUseCase = Mockito.mock(ListUsersUseCase::class.java)
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @get:Rule
    val rule = InstantTaskExecutorRule()


    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    init {
        startKoin {  listOf(remoteModule, repositoryModule, uiModule, servicesModule) }
        declare {
            single { listUsersUseCase }
        }
    }

    @After
    fun tearDown(){
        stopKoin()
    }

    @Test
    fun verifyIfIValidList() {
        val viewModel = viewModel()
        runBlocking {
            `when` (listUsersUseCase.execute(1)).thenReturn(Resource.success(C6ServiceMock().getRepos(1).body()))
            viewModel.repo.test()
                .awaitValue()
                .assertHasValue()
        }
    }

    @Test
    fun verifyIfObserverReceiveValue() {
        val viewModel = viewModel()
        runBlocking {
            `when` (listUsersUseCase.execute(1)).thenReturn(Resource.success(C6ServiceMock().getRepos(1).body()))
            viewModel.repo.test()
                .awaitValue()
                .assertHasValue()
                .assertValue { it != null }

        }
    }
}