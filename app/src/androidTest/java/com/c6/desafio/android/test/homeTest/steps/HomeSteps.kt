package com.c6.desafio.android.test.homeTest.steps

import androidx.test.rule.ActivityTestRule
import com.c6.desafio.android.home.MainActivity
import com.c6.desafio.android.test.common.ActivityFinisher
import com.c6.desafio.android.test.homeTest.robot.RobotHome
import cucumber.api.java.After
import cucumber.api.java.Before
import cucumber.api.java.en.And
import cucumber.api.java.en.Given

class HomeSteps {

    private val activityRule = ActivityTestRule(MainActivity::class.java, false, false)

    private val robot = RobotHome()

    @Before
    fun setup() {

    }

    @After
    fun tearDown() {
        ActivityFinisher.finishOpenActivities()
    }

    @Given("^Que eu inicio a tela de home$")
    fun openScreenLogin() {
        robot.launchLoginScreen(activityRule)
    }

    @And("^Eu vejo o Titulo da minha tela$")
    fun validTitle() {
        robot.shouldDisplayTitle(activityRule.activity)
    }

    @And("^Eu vejo a lista de usuarios do git$")
    fun validList() {
        robot.shouldDisplayListItem()
    }


}