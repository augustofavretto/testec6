package com.c6.desafio.android.test.homeTest.robot

import android.content.Context
import androidx.test.rule.ActivityTestRule
import com.c6.desafio.android.R
import com.c6.desafio.android.home.MainActivity
import com.c6.desafio.android.test.common.ScreenRobot
import com.c6.desafio.android.test.homeTest.constants.HomeConstants
import okhttp3.mockwebserver.MockWebServer


class RobotHome {

    private val robot = ScreenRobot()
    private val server = MockWebServer()

    fun launchLoginScreen(testRule: ActivityTestRule<MainActivity>) {
        testRule.launchActivity(null)
    }

    fun shouldDisplayTitle(context:Context) {
        robot.checkViewContainsText(context.getString(R.string.title))
    }

    fun shouldDisplayListItem(){
        robot.checkViewContainsText(HomeConstants.USER_NAME)
    }

    companion object {
        private val VIEW_TITLE = R.id.title
    }
}