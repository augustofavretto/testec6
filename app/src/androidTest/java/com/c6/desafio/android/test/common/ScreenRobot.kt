package com.c6.desafio.android.test.common

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*


class ScreenRobot {

    fun checkIsDisplayed(@IdRes vararg viewIds: Int) {
        for (viewId in viewIds) {
            onView(withId(viewId)).check(matches(isDisplayed()))
        }
    }

    fun checkViewContainsText(text: String) {
        onView(withText(text)).check(matches(isDisplayed()))
    }

}

