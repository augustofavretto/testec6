package com.c6.desafio.android.test.homeTest.constants

import com.c6.desafio.android.home.service.UserMock


object HomeConstants {
    const val USER_NAME = UserMock.NAME
}