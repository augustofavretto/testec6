package com.c6.desafio.android.home.service

object UserMock {
    const val NAME = "architecture-samples"
    const val USER_AVATAR = "https://avatars.githubusercontent.com/u/32689599?v=4"
    const val USER_ID = 51148780
    const val SART = 38578
    const val FORK = 10635
    const val FULL_NAME= "android/architecture-samples"
}