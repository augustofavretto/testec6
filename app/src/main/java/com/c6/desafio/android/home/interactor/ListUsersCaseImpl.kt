package com.c6.desafio.android.home.interactor

import com.c6.desafio.android.common.Resource
import com.c6.desafio.android.home.model.ResponseListRepo
import com.c6.desafio.android.home.service.C6Service

class ListUsersCaseImpl(
    private val service: C6Service
) : ListUsersUseCase() {

    override suspend fun execute(params: Int): Resource<ResponseListRepo> {
        return try {
            val response = service.getRepos(page = params)
            when {
                response.isSuccessful -> {
                    response.body().let {
                        Resource.success(it)
                    }
                }
                else -> Resource.error(Exception(response.message()))
            }

        } catch (e: Exception) {
            Resource.error(e)
        }
    }
}