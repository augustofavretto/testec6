package com.c6.desafio.android.di

import com.c6.desafio.android.home.service.C6Service
import com.c6.desafio.android.home.MainViewModel
import com.c6.desafio.android.home.interactor.ListUsersCaseImpl
import com.c6.desafio.android.home.interactor.ListUsersUseCase
import com.c6.desafio.android.home.service.C6ServiceMock
import com.c6.desafio.android.utils.extensions.resolveRetrofit
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val remoteModule = module {
    single { provideOkHttpClient() }
    single { provideRetrofit(get()) }
}

val servicesModule = module {
    factory<C6Service> {
        resolveRetrofit() ?: C6ServiceMock()
    }

}

val uiModule = module {
    viewModel { MainViewModel( get()) }
}

val repositoryModule = module {
    factory<ListUsersUseCase> { ListUsersCaseImpl(get()) }
}



