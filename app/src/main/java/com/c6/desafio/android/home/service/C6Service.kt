package com.c6.desafio.android.home.service

import com.c6.desafio.android.home.model.ResponseListRepo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface C6Service {

    @GET("search/repositories?q=language:kotlin&sort=stars")
    suspend fun getRepos(
        @Query("page") page: Int
    ): Response<ResponseListRepo>
}