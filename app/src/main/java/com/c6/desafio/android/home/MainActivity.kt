package com.c6.desafio.android.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.c6.desafio.android.R
import com.c6.desafio.android.common.Resource
import com.c6.desafio.android.home.adapter.UserListAdapter
import com.c6.desafio.android.utils.PaginationScrollListener
import com.c6.desafio.android.utils.Util
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    private lateinit var adapter: UserListAdapter
    private val viewModel by viewModel<MainViewModel>()
    private var isloading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupObservables()
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        uiRecyclerView.apply {
            val layout = LinearLayoutManager(context)
            layoutManager = layout
            this@MainActivity.adapter = UserListAdapter()
            adapter = this@MainActivity.adapter
            uiRecyclerView?.addOnScrollListener(object : PaginationScrollListener(layout) {
                override fun isLoadMorePossible(): Boolean {
                    return viewModel.isPossible()
                }

                override fun isLoading(): Boolean {
                    return isloading
                }

                override fun loadMoreItems() {
                    launch { viewModel.fetchUsers() }
                }
            })

        }
    }

    private fun setupObservables() {
        viewModel.repo.observe(this, { resource ->
            when (resource?.status) {
                Resource.Status.SUCCESS -> {
                    uiProgressBar.visibility = View.GONE
                    resource.data?.let {
                            listUsers -> adapter.users = listUsers.mRepos
                            isloading = false
                    }
                }
                Resource.Status.LOADING -> {
                    uiProgressBar.visibility = View.VISIBLE
                    isloading = true
                }
                Resource.Status.ERROR -> {
                    Util.showShortToastMessage(this, getString(R.string.error))
                    uiProgressBar.visibility = View.GONE
                    isloading = false
                }
                else -> {
                }
            }
        })
    }
}
