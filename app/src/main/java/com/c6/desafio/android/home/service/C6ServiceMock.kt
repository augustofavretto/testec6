package com.c6.desafio.android.home.service

import com.c6.desafio.android.home.model.Owner
import com.c6.desafio.android.home.model.ResponseListRepo
import com.c6.desafio.android.home.model.User
import com.c6.desafio.android.home.service.UserMock.FORK
import com.c6.desafio.android.home.service.UserMock.SART
import com.c6.desafio.android.home.service.UserMock.USER_AVATAR
import com.c6.desafio.android.home.service.UserMock.USER_ID
import com.c6.desafio.android.home.service.UserMock.NAME
import com.c6.desafio.android.home.service.UserMock.FULL_NAME
import retrofit2.Response

class C6ServiceMock : C6Service {

    override suspend fun getRepos(page: Int): Response<ResponseListRepo> {
        return Response.success(ResponseListRepo(
            totalSize = 1,
            mRepos = listOf(
                User(Owner(USER_AVATAR),NAME,USER_ID,SART,FORK,FULL_NAME)
            ).toMutableList()))
    }
}