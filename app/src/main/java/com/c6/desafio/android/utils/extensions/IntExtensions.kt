package com.c6.desafio.android.utils.extensions

fun Int.formatForLabel(): String{
    var numberString = ""
    val number = this
    numberString = if (Math.abs(number / 1000000) > 1) {
        (number / 1000000).toString() + "m"
    } else if (Math.abs(number / 1000) > 1) {
        (number / 1000).toString() + "k"
    } else {
        number.toString()
    }
    return numberString
}