package com.c6.desafio.android

import android.app.Application
import com.c6.desafio.android.di.remoteModule
import com.c6.desafio.android.di.repositoryModule
import com.c6.desafio.android.di.servicesModule
import com.c6.desafio.android.di.uiModule
import org.koin.core.context.startKoin


open class App : Application() {

    private val appModules by lazy {
        listOf(remoteModule, repositoryModule,uiModule, servicesModule)
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(appModules)
        }

    }

}