package com.c6.desafio.android.home.interactor

import com.c6.desafio.android.common.Resource
import com.c6.desafio.android.common.UseCase
import com.c6.desafio.android.home.model.ResponseListRepo

abstract class ListUsersUseCase : UseCase<Int, Resource<ResponseListRepo>>()