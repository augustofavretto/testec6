package com.c6.desafio.android.home.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.c6.desafio.android.R
import com.c6.desafio.android.home.model.User
import com.c6.desafio.android.utils.extensions.formatForLabel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item_user.view.*

class UserListItemViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(user: User) {
        itemView.name.text = user.name
        itemView.username.text = user.username
        itemView.uiSart.text = user.star.formatForLabel()
        itemView.uiForck.text = user.fork.formatForLabel()
        itemView.progressBar.visibility = View.VISIBLE
        Picasso.get()
            .load(user.owner.img)
            .error(R.drawable.ic_round_account_circle)
            .into(itemView.picture, object : Callback {
                override fun onSuccess() {
                    itemView.progressBar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    itemView.progressBar.visibility = View.GONE
                }
            })
    }
}