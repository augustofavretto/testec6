package com.c6.desafio.android.utils

import android.content.Context
import android.widget.Toast

object Util {
    fun showShortToastMessage(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}