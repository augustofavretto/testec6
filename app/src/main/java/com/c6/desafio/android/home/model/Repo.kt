package com.c6.desafio.android.home.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    @SerializedName("owner") val owner: Owner,
    @SerializedName("name") val name: String,
    @SerializedName("id") val id: Int,
    @SerializedName("stargazers_count") var star: Int,
    @SerializedName("forks_count") val fork: Int,
    @SerializedName("full_name") val username: String
) : Parcelable


@Parcelize
data class Owner(
    @SerializedName("avatar_url") val img: String
) : Parcelable