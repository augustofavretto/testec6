package com.c6.desafio.android.common

import com.c6.desafio.android.home.model.User
import retrofit2.Response

class Resource<T> private constructor(
    val status: Status,
    val data: T? = null,
    val exception: Throwable? = null
) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data)
        }

        fun <T> error(exception: Throwable?, data: T? = null): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                exception
            )
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(
                Status.LOADING,
                data
            )
        }
    }
}