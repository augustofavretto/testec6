package com.c6.desafio.android.home.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseListRepo(
    @SerializedName("total_count") val totalSize: Int,
    @SerializedName("items") var mRepos: MutableList<User> = mutableListOf()
) : Parcelable