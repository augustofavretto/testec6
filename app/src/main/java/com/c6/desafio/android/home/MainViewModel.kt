package com.c6.desafio.android.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.c6.desafio.android.common.Resource
import com.c6.desafio.android.home.interactor.ListUsersUseCase
import com.c6.desafio.android.home.model.ResponseListRepo
import com.c6.desafio.android.home.model.User
import kotlinx.coroutines.launch

class MainViewModel(
    private val listUseCase: ListUsersUseCase

) : ViewModel() {
    private var _listRepo: MutableLiveData<Resource<ResponseListRepo>> = MutableLiveData()
    private val currentPage: MutableLiveData<Int> = MutableLiveData()
    private val totalSizeList: MutableLiveData<Int> = MutableLiveData()
    private var currentList: MutableLiveData<MutableList<User>> = MutableLiveData()

    val repo: LiveData<Resource<ResponseListRepo>> get() = _listRepo

    init {
        currentList.value = mutableListOf()
        currentPage.value = 0
        viewModelScope.launch {
            fetchUsers()
        }
    }

    suspend fun fetchUsers() {
        _listRepo.postValue(Resource.loading())
        currentPage.value = currentPage.value?.let { it + 1 }
        currentPage.value?.let { it ->
            val result = listUseCase.execute(it)
            totalSizeList.value = result.data?.totalSize
            result.data?.let { resultRequest -> addMore(result, resultRequest) }
            _listRepo.postValue(result)
        }
    }

    private fun addMore(
        result: Resource<ResponseListRepo>, data: ResponseListRepo
    ) {
        currentList.value?.let { currentList ->
            currentList.addAll(data.mRepos)
            result.data?.mRepos = currentList
        }
    }

    fun isPossible(): Boolean {
        return currentList.value?.size != totalSizeList.value
    }


}
